package com.proyecto.optionals;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Optional;

import com.proyecto.optionals.logic.GameSearch;

public class App {

	public static GameSearch gameSearch;

	public static void main(String[] args) {
		gameSearch = new GameSearch();
		int idJuego = 0;
		System.out.println("Bienvenido a la videoteca, introduce el id de un juego para obtener los datos");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {
			idJuego = Integer.parseInt(reader.readLine());
		} catch (Exception e) {
			System.out.println("Error al obtener el valor introducido");
		}
		searchVideoGame(idJuego);
	}

	public static void searchVideoGame(Integer id) {
		Optional<VideoGame> optionalGame = gameSearch.searchGame(id);

		optionalGame.ifPresentOrElse(game -> {
			System.out.println("Title: " + game.getName());
			System.out.println("Genre: " + game.getGenre().get().getName());
			System.out.print("Plataform: " + game.getPlatform().get().getName());
			game.getPlatform().get().getMaker().ifPresent(maker -> System.out.print(" (" + maker.getName() + ")"));
		}, () -> System.out.println("No se ha encontrado ningún juego con ese id"));
	}

}
