package com.proyecto.optionals.logic;

import java.time.LocalDate;
import java.util.Optional;

import com.proyecto.optionals.Genre;
import com.proyecto.optionals.Maker;
import com.proyecto.optionals.Platform;
import com.proyecto.optionals.VideoGame;


public class GameSearch {
	private Optional<Platform> platform;
	private Optional<Genre> genre;
	private LocalDate releaseDate;
	private Optional<Maker> maker;
	
	public GameSearch() {
		
	}
	public Optional<VideoGame> searchGame(Integer id) {

		switch (id) {

		case 1:
			this.platform = Optional.of(new Platform(1,"PC",Optional.empty()));
			this.genre = Optional.of(new Genre(1,"graphic adventure"));
			this.releaseDate = LocalDate.of(1998,10,30);
			return Optional.of(new VideoGame(1, "Grim Fandango",platform,genre,releaseDate));
		case 2:
			this.maker = Optional.of(new Maker(1,"Microsoft"));
			this.platform = Optional.of(new Platform(2,"Xbox 360",maker));
			this.genre = Optional.of(new Genre(2,"FPS"));
			this.releaseDate = LocalDate.of(2007,9,25);
			return Optional.of(new VideoGame(2, "Halo 3",platform,genre,releaseDate));
		case 3:
			this.maker = Optional.of(new Maker(2,"Sony"));
			this.platform = Optional.of(new Platform(3,"Playstation 3",maker));
			this.genre = Optional.of(new Genre(4,"Adventure"));
			this.releaseDate = LocalDate.of(2013,6,14);
			return Optional.of(new VideoGame(3, "The last of us",platform,genre,releaseDate));
		case 4:
			this.maker = Optional.of(new Maker(2,"Sony"));
			this.platform = Optional.of(new Platform(3,"Playstation 1",maker));
			this.genre = Optional.of(new Genre(4,"Horror"));
			this.releaseDate = LocalDate.of(1999,1,31);
			return Optional.of(new VideoGame(4, "Silent Hill",platform,genre,releaseDate));
		case 5:
			this.maker = Optional.of(new Maker(3,"Nintendo"));
			this.platform = Optional.of(new Platform(4,"Nintendo Switch",maker));
			this.genre = Optional.of(new Genre(5,"RPG"));
			this.releaseDate = LocalDate.of(2022,7,29);
			return Optional.of(new VideoGame(5, "Xenoblade Chronicles 3",platform,genre,releaseDate));
		case 6:
			this.maker = Optional.of(new Maker(2,"Sony"));
			this.platform = Optional.of(new Platform(4,"Playstation 4",maker));
			this.genre = Optional.of(new Genre(5,"RPG"));
			this.releaseDate = LocalDate.of(2015,3,24);
			return Optional.of(new VideoGame(6, "Bloodborne",platform,genre,releaseDate));
		case 7:
			this.maker = Optional.of(new Maker(2,"Sony"));
			this.platform = Optional.of(new Platform(5,"Playstation 5",maker));
			this.genre = Optional.of(new Genre(5,"RPG"));
			this.releaseDate = LocalDate.of(2022,2,25);
			return Optional.of(new VideoGame(7, "Elden Ring",platform,genre,releaseDate));
		case 8:
			this.maker = Optional.of(new Maker(2,"Sony"));
			this.platform = Optional.of(new Platform(6,"Playstation 2",maker));
			this.genre = Optional.of(new Genre(4,"Horror"));
			this.releaseDate = LocalDate.of(2006,1,19);
			return Optional.of(new VideoGame(8, "Rule of Roses",platform,genre,releaseDate));
		default:
			return Optional.empty();
		}

	}
}
