package com.proyecto.optionals;

import java.util.Optional;

public class Platform {

	private Integer id;
	private String name;
	private Optional<Maker> maker;
	
	
	public Platform() {
	}
	
	public Platform(Integer id, String name, Optional<Maker> maker) {
		super();
		this.id = id;
		this.name = name;
		this.maker = maker;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Optional<Maker> getMaker() {
		return maker;
	}
	public void setMaker(Optional<Maker> maker) {
		this.maker = maker;
	}
	
	
	
}
