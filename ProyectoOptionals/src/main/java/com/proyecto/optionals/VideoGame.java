package com.proyecto.optionals;

import java.time.LocalDate;
import java.util.Optional;

public class VideoGame {
	
	private Integer id;
	private String name;
	private Optional<Platform> platform;
	private Optional<Genre> genre;
	private LocalDate releaseDate;
	
	public VideoGame() {
	}

	public VideoGame(Integer id, String name, Optional<Platform> platform, Optional<Genre> genre,
			LocalDate releaseDate) {
		super();
		this.id = id;
		this.name = name;
		this.platform = platform;
		this.genre = genre;
		this.releaseDate = releaseDate;
	}


	public LocalDate getReleaseDate() {
		return releaseDate;
	}


	public void setReleaseDate(LocalDate releaseDate) {
		this.releaseDate = releaseDate;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Optional<Platform> getPlatform() {
		return platform;
	}

	public void setPlatform(Optional<Platform> platform) {
		this.platform = platform;
	}

	public Optional<Genre> getGenre() {
		return genre;
	}

	public void setGenre(Optional<Genre> genre) {
		this.genre = genre;
	}
	

	
	
	
}
