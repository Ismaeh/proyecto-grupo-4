package com.practica.time;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;

public class AppVuelos {

	private static ZoneId origen;
	private static ZoneId destino;
	private static ZonedDateTime zoneDateOrigen;
	private static ZonedDateTime zoneDateDestination;

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		MainMenu();

	}

	private static void MainMenu() throws IOException {

		String selection = "";

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("***************BIENVENIDO A LA AGENCIA DE VUELOS************");
		System.out.println("*  ¿Qué desea consultar?:				   *");
		System.out.println("*  1: Fecha actual					   *");
		System.out.println("*  2: Crear un vuelo					   *");
		System.out.println("*  3: Hora en el pais destino			           *");
		System.out.println("*  4: Duración del vuelo				   *");
		System.out.println("*  5: Diferencia horaria				   *");
		System.out.println("*  6: Salir					           *");
		System.out.println("************************************************************");
		selection = br.readLine();

		switch (selection) {
		case "1":
			checkActualDate();
			break;
		case "2":
			createNewFlight();
			break;
		case "3":
			timeInDestination();
			break;
		case "4":
			flightDuration();
			break;
		case "5":
			diferenciaHoraria();
			break;
		case "6":
			System.out.println("Gracias por usar nuestra agencia, Adiós.");
			System.exit(0);
			break;
		default:
			System.out.println("Por favor seleccione alguna de las opciones escribiendo solo un número");
			MainMenu();
			break;
		}

	}

	private static void checkActualDate() throws IOException {

		LocalDate localDate = LocalDate.now();
		LocalTime localTime = LocalTime.now();

		System.out.println("La fecha y hora actual es: " + localDate + " / " + localTime);
		System.out.println("/************************************************/");
		System.out.println("");
		System.out.println("");
		System.out.println("");

		MainMenu();

	}

	private static void createNewFlight() throws IOException {

		String selection;
		String selection2;

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("***************NUEVO VUELO********************");
		System.out.println("Seleccione el origen:");
		System.out.println("1: Madrid");
		System.out.println("2: Melilla/Ceuta");
		System.out.println("3: Canarias");
		System.out.println("4: Volver al menú principal");
		System.out.println("************************************************************");
		selection = br.readLine();

		switch (selection) {

		case "1":
			origen = ZoneId.of("Europe/Madrid");
			break;
		case "2":
			origen = ZoneId.of("Africa/Ceuta");
			break;
		case "3":
			origen = ZoneId.of("Atlantic/Canary");
			break;
		case "4":
			MainMenu();
			break;
		default:
			createNewFlight();
			break;
		}

		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("***************NUEVO VUELO********************");
		System.out.println("Seleccione el destino:");
		System.out.println("1: Mexico-Tijuana");
		System.out.println("2: Canadá-Vancouver");
		System.out.println("3: EEUU-Denver");
		System.out.println("4: Volver al menú principal");
		System.out.println("************************************************************");
		selection2 = br2.readLine();

		switch (selection2) {

		case "1":
			destino = ZoneId.of("America/Tijuana");
			break;
		case "2":
			destino = ZoneId.of("America/Vancouver");
			break;
		case "3":
			destino = ZoneId.of("America/Denver");
			break;
		case "4":
			MainMenu();
			break;
		default:
			createNewFlight();
			break;
		}

		MainMenu();

	}

	private static void timeInDestination() throws IOException {

		if (origen != null && destino != null) {
			zoneDateOrigen = ZonedDateTime.of(LocalDateTime.now(), origen);

			System.out.println("La hora actual en el destino seleccionado es: " + zoneDateOrigen);
		} else {
			System.out.println("Antes debe crear un vuelo");
		}

		MainMenu();
	}

	private static void flightDuration() throws IOException {

		if (origen != null && destino != null) {
			zoneDateOrigen = ZonedDateTime.of(LocalDateTime.now(), origen);
			zoneDateDestination = zoneDateOrigen.withZoneSameInstant(destino);

			Integer duration = zoneDateDestination.getHour() - zoneDateOrigen.getHour();

			Integer durationFlight = (int) (Math.random() * ((duration + 5) - (duration - 2)) + (duration - 2));

			Duration d = Duration.of(durationFlight, ChronoUnit.HOURS);

			System.out.println("La duración del vuelo  " + origen + " y " + destino + " es de: " + Math.abs(d.toHours())
					+ " horas");
		} else {
			System.out.println("Antes debe crear un vuelo");
		}

		MainMenu();

	}

	private static void diferenciaHoraria() throws IOException {

		if (origen != null && destino != null) {

			zoneDateOrigen = ZonedDateTime.of(LocalDateTime.now(), origen);
			zoneDateDestination = zoneDateOrigen.withZoneSameInstant(destino);

			Integer duration = zoneDateDestination.getHour() - zoneDateOrigen.getHour();

			System.out.println("La diferencia horaria entre " + origen + " y " + destino + " sería de: "
					+ Math.abs(duration) + " horas");
		} else {
			System.out.println("Antes debe crear un vuelo");
		}

		MainMenu();
	}

}
